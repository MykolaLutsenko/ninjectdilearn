﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ninject;
using NinjectDILearn.Models;

namespace NinjectDILearn.Controllers
{
    public class HomeController : Controller
    {
        private IValueCalculator calc;

        private Product[] products = {
            new Product {Name = "Каяк", Category = "Водные виды спорта", Price = 275M},
            new Product {Name = "Спасательный жилет", Category = "Водные виды спорта", Price = 48.95M},
            new Product {Name = "Мяч", Category = "Футбол", Price = 19.50M},
            new Product {Name = "Угловой флажок", Category = "Футбол", Price = 34.95M}
        };

        public HomeController(IValueCalculator calcParam, IValueCalculator calc2)
        {
            calc = calcParam;
        }

        // GET: Home
        public ActionResult Index()
        {
            ////создаем стандартное ядро нинжект.
            //IKernel ninjectKernel = new StandardKernel();
            
            ////конфигурируем ядро. показываем какие обьекты реализации привязаны 
            ////для каждого интерфейса.
            //ninjectKernel.Bind<IValueCalculator>().To<LinqValueCalculator>();

            ////действительное испрользование посредством метода get() ядра
            //IValueCalculator calc = ninjectKernel.Get<IValueCalculator>();

            ShoppingCart cart = new ShoppingCart(calc) { 
                Products = products 
            };
            decimal totalValue = cart.CalculateProductTotal();
            return View(totalValue);
        }
    }
}