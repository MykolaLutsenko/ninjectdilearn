﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NinjectDILearn.Models
{
    //DI Injections
    public class ShoppingCart
    {
        private readonly IValueCalculator _calc;

        public ShoppingCart(IValueCalculator calcParam)
        {
            this._calc = calcParam;
        }

        public IEnumerable<Product> Products { get; set; }

        public decimal CalculateProductTotal()
        {
            return _calc.ValueProducts(Products);
        }
    }
}