﻿using System.Collections.Generic;

namespace NinjectDILearn.Models
{
    public interface IValueCalculator
    {
        decimal ValueProducts(IEnumerable<Product> products);
    }
}